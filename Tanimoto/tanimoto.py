# -*- coding: utf-8 -*-

#Написать функцию для расчета коэффициента похожести
#двух слов по формуле Танимото

#import re

def tanimoto(a, b):
    a = a.lower()
    b = b.lower()
    
    Nc = 0
    
    for i in a:
        if i in b:
            Nc +=1
    
    return Nc/(len(a) + len(b) - Nc)


k = tanimoto('raccoon', 'RaCCooN')
print(k)

k = tanimoto('abc', 'cdef')
print(k)

