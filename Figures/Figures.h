﻿#ifndef FIGURES
#define FIGURES

#include<iostream>
#include<cmath>
#include<vector>
const double pi = 3.1415926535897932;
using namespace std;

class Figure{
public:
	virtual double getArea() = 0;
	virtual double getPerimeter() = 0;
};


template <class T>class Square :public Figure {
private:
	T a;
public:
	Square() {a, b = 0;};
	Square(T _a) {a = _a;}

	~Square() {};

	virtual T getArea() {return a*a;}

	virtual T getPerimeter(){return 4 * a;}
};


template <class T> class Rectangle :public Figure {

private:
	T a, b;
public:
	Rectangle() {a, b = 0;}
	Rectangle(T _a, T _b) {a = _a; b = _b;}

	~Rectangle() {};

	virtual T getArea() {
		return a*b; 
	}

	virtual T getPerimeter() {
		return 2*(a + b); 
	}
};


template <class T> class Circle : public Figure{
private:
	T r;
public:
	Circle() { r = 0; }
	Circle(T _r) { r = _r; };
	~Circle() {};

	virtual T getArea() {
		return r*r*pi;
	}

	virtual T getPerimeter() {
		return 2 * pi*r;
	}
};

template <class T> class Triangle : public Figure {
private:
	T a, b, c, p;

public:
	Triangle() { a, b, c = 0; }
	Triangle(T _a, T _b, T _c) { a = _a; b = _b; c = _c; p = (a + b + c) / 2; }
	~Triangle() {};

	virtual T getPerimeter() {
		return p*2;
	}

	virtual T getArea() {
		return sqrt(p*(p - a)*(p - b)*(p - c));
	}
};

template <class T> class Ellips : public Figure {
private:
	T a, b;

public:
	Ellips() { a, b = 0; };
	Ellips(T _a, T _b) { a = _a; b = _b; };
	~Ellips() {};

	T getPerimeter() {
		return 2*pi*sqrt(a*a + b*b/8);
	}

	T getArea() {
		return pi*a *b ;
	}
};

template <class T> class Polygon : public Figure {
private:
	vector<T> x;
	vector<T> y;
public:
	Polygon() { vector<T> x, vector<T> y = 0; };
	Polygon(vector<T> _x, vector<T> _y) { x =  _x; y = _y; };
	~Polygon() {};

	T getPerimeter() {
		int N = x.size();
		T p = 0;
		for (int i = 0; i < N - 1; ++i)
			p += sqrt((x[i] - x[i + 1])*(x[i] - x[i + 1]) + (y[i] - y[i + 1])*(y[i] - y[i + 1]));
		p += sqrt((x[0] - x[N - 1])*(x[0] - x[N - 1]) + (y[0] - y[N - 1])*(y[0] - y[N - 1]));
		return p;
	}

	T getArea() {
		int N = x.size();
		T area = 0;
		for (int i = 0; i<N-1; i++)
			area += (x[i] - x[i + 1])*(y[i] + y[i + 1]);		
		area = area + (x[N-1] - x[0])*(y[N-1] + y[0]);
		return abs(area / T(2));
	}
};



#endif
