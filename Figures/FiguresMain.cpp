#include"Figures.h"
#include<conio.h>
#include<locale>

//double pArea(Figure *mFig);

int main(void) {
	setlocale(LC_ALL, "Russian");

	double a = 5.0;
	double b = 6.0;

	cout << "a = 5, b = 6" << endl;

	Square<double> *mSquare = new Square<double>(a);
	cout << endl << "mSquare.getArea(): " << mSquare->getArea() << endl;
	cout << "mSquare.getPerimeter(): " << mSquare->getPerimeter() << endl;

	Rectangle<double> *mRectangle = new Rectangle<double>(a, b);
	cout << endl  << "mRectangle.getArea(): " << mRectangle->getArea() << endl;
	cout << "mRectangle.getPerimeter(): " << mRectangle->getPerimeter() << endl;

	cout << endl << "r = 7" << endl;
	double r = 7.0;

	Circle<double> *mCircle = new Circle<double>(r);
	cout << endl << "mCircle.getArea(): " << mCircle->getArea() << endl;
	cout << "mCircle.getPerimeter(): " << mCircle->getPerimeter() << endl;

	cout << endl << "a=5, b=6, c=7";
	Triangle<double> *mTriangle = new Triangle<double>(a, b, r);
	cout << endl << "mTriangle.getArea(): " << mTriangle->getArea() << endl;
	cout << "mTriangle.getPerimeter(): " << mTriangle->getPerimeter() << endl;

	cout << endl << "a=5, b=6";
	Ellips<double> *mEllips = new Ellips<double>(a, b);
	cout << endl << "mEllips.getArea(): " << mEllips->getArea() << endl;
	cout << "mEllips.getPerimeter(): " << mEllips->getPerimeter() << endl;


	vector<double> x;
	vector<double> y;

	x.push_back(1);
	x.push_back(5);
	x.push_back(5);
	x.push_back(2);
	x.push_back(0);

	y.push_back(2);
	y.push_back(2);
	y.push_back(-1);
	y.push_back(-3);
	y.push_back(-1);

	cout << endl << "vector X={1, 5, 5, 2, 0}" << endl;
	cout << "vector Y={2, 2, -1, -3, -1}" << endl;


	Polygon<double> *mPolygon = new Polygon<double>(x, y);
	cout << endl << "mPolygon.getArea(): " << mPolygon->getArea() << endl;
	cout << "mPolygon.getPerimeter(): " << mPolygon->getPerimeter() << endl;


	double sum = 0;

	Figure *mFigures = mSquare;
	sum += mFigures->getArea();

	mFigures = mRectangle;
	sum += mFigures->getArea();


	mFigures = mCircle;
	sum += mFigures->getArea();

	mFigures = mTriangle;
	sum += mFigures->getArea();

	mFigures = mPolygon;
	sum += mFigures->getArea();

	cout << endl << "��������� �������: " << sum;
	
	_getch();
	return 0;
}

