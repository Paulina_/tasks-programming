# -*- coding: utf-8 -*-

#Пользователь вводит слово на английском языке,
#программа выдает количество гласных и согласных букв

import re

def count_letters(word):
    word = re.sub(re.compile(r'[^a-z]', re.I), '', word)
    vowels = len(re.findall(r'[aeiouy]', word, re.I))
    consonants = len(word) - vowels
    return vowels, consonants

while True:
    text = input("Input the word: ")
    
    if text == 'exit':
        break
    
    v, c = count_letters(text)
    print('Vowels: ' + str(v), 'Consonants: ' + str(c))
    
    
