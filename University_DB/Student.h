#ifndef STUDENT
#define STUDENT

#include"Human.h"
#include<vector>

class Student :public Human{
private:
	vector<int> marks; 
	string formOfTraining; 

public:

	Student() :Human() {};

	Student(string _lastName, string _name, string _secondName, vector<int> _marks, int _age,
		string _university,  string _gender, string _formOfTraining) :
		Human(_lastName, _name, _secondName, _age, _university, _gender) {
		this->marks = _marks;
		this->formOfTraining = formOfTraining;
	}


	string getOccupation() const;

	float getAverageMarks();
	virtual string getFormOfTraining();
};

#endif