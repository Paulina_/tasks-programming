#include "Human.h"

using namespace std;

Human::Human(string _lastName, string _name, string _secondName, int _age, string _university, string _gender) {
	this->lastName = _lastName;
	this->name = _name;
	this->secondName = _secondName;
	this->age = _age;
	this->university = _university;
	this->gender = _gender;
}

string Human::getFullName() const {
	ostringstream fullName;
	fullName << this->lastName << " "
		<< this->name << " "
		<< this->secondName;
	return fullName.str();
}

int Human::getAge() const {
	return this->age;
}

string Human::getUniversity() const {
	return this->university;
}

string Human::getGender() const {
	return this->gender;
}

string Human::getName() const {
	return this->name;
}

string Human::getLastName() const {
	return this->lastName;
}

string Human::getSecondName() const {
	return this->secondName;
}

void Human::setName(string _name){
	this->name = _name;
}

void Human::setLastName(string _lastName) {
	this->name = _lastName;
}

void Human::setSecondName(string _secondName) {
	this->name = _secondName;
}

void Human::setUniversity(string _university) {
	this->name = _university;
}

void Human::setOccupation(string _occupation) {
	this->name = _occupation;
}

void Human::setGender(string _gender) {
	this->name = _gender;
}