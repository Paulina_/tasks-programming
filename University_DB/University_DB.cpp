#include"Human.h"
#include"Student.h"
#include"Teacher.h"
#include<locale>
#include<conio.h>


int main(void) {
	setlocale(LC_ALL, "Russian");

	vector<int> marks;

	marks.push_back(5);
	marks.push_back(4);
	marks.push_back(5);
	marks.push_back(4);
	marks.push_back(5);
	
	Student *mStudent = new Student("Miller", "Jones", "Brown", marks, 18, "ISU","male", "full-time");

	cout << "mStudent->getFullName: " << mStudent->getFullName() << endl;
	cout << "mStudent->getAverageMarks: " << mStudent->getAverageMarks() << endl;
	cout << "mStudent->getAge: " << mStudent->getAge() << endl;
	cout << "mStudent->getUniversity: " << mStudent->getUniversity() << endl;
	cout << "mStudent->getOccupation: " << mStudent->getOccupation() << endl;
	cout << "mStudent->getGender: " << mStudent->getGender() << endl;
	cout << "mStudent->getGender: " << mStudent->getFormOfTraining() << endl;

	Teacher *mTeacher = new Teacher("Miller", "Jones", "Brown", 58, "ISU",
		"male", "Chair of journalism", "Lecturer");

	cout << endl << "mTeacher->getFullName: " << mTeacher->getFullName() << endl;
	cout << "mTeacher->getAge: " << mTeacher->getAge() << endl;
	cout << "mTeacher->getUniversity: " << mTeacher->getUniversity() << endl;
	cout << "mTeacher->getGender: " << mTeacher->getGender() << endl;
	cout << "mTeacher->getDepartment: " << mTeacher->getDepartment() << endl;
	cout << "mTeacher->getPosition: " << mTeacher->getPosition() << endl;
	cout << "mTeacher->setName('Tom'): " << endl;
	mTeacher->setName("Tom");
	cout << "mTeacher->getFullName: " << mTeacher->getFullName() << endl;

	delete mStudent;
	delete mTeacher;

	_getch();
	return(0);
}