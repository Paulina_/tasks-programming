#ifndef HUMAN
#define HUMAN

#include <iostream>
#include <sstream>
#include <string>

using namespace std;

class Human {
public:
	string name;
	string lastName;
	string secondName;
	int age;
	string university;
	string occupation;
	string gender;

	virtual ~Human() {};
	virtual string getOccupation() const = 0;

	Human() {};
	Human(string _lastName, string _name, string _secondName, int _age, string _universit,  string _gender);


	int getAge() const;
	string getFullName() const;
	string getName() const;
	string getLastName() const;
	string getSecondName() const;
	string getUniversity() const;	
	string getGender() const;

	void setName(string _name);
	void setLastName(string _lastName);
	void setUniversity(string _university);
	void setOccupation(string _occupation);
	void setGender(string _gender);
	void setSecondName(string _SecondName);

};

#endif
