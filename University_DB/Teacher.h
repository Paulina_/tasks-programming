#ifndef TEACHER
#define TEACHER

#include"human.h"

class Teacher :public Human {
private:
	string department;
	string position;
public:
	Teacher() :Human() {};

	Teacher(string _lastName, string _name, string _secondName, int _age,
		string _university, string _gender, string _department, string _position) :
		Human(_lastName, _name, _secondName, _age, _university, _gender) {
		this->department = _department;
		this->position = _position;
	}

	virtual string getOccupation() const;

	string getDepartment() const;
	string getPosition() const;

	void setDepartment(string _department);
	void setPosition(string _position);
};

#endif