#include "Student.h"

virtual string Student::getOccupation()const {
	return "Student";
}

float Student::getAverageMarks()
{
	unsigned int countMarks = this->marks.size();
	unsigned int sumMarks = 0;
	float averageMarks = 0;

	for (unsigned int i = 0; i < countMarks; i++)
		sumMarks += this->marks[i];

	averageMarks = float(sumMarks) / float(countMarks);

	return averageMarks;
}
string Student::getFormOfTraining() {
	return this->formOfTraining;
}