# -*- coding: utf-8 -*-


#Составить частотный словарь(частота встречаемости
#каждого символа) для текста из файла lorem.txt


def load(path):
    m_dict = {}
    
    with open(path) as f:
        for line in f.read().split('\n'):
            for ch in line:
                ch = str(ch.lower())
                if ch in m_dict:
                    value = m_dict[ch] + 1
                else:
                    value = 1
                m_dict[ch] = value
    
    return m_dict

m_dict = load('lorem.txt')
print(m_dict)