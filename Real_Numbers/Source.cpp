/*   Сгенерировать случайный список из 10 вещественных
чисел(-100 до 100), который необходимо отсортировать по
возрастанию дробной части     */

#include<iostream>
#include<time.h>
#include<conio.h>

using namespace std;

double m_rand(double l, double r);
void m_sort(double *data, int len);
double get_fraction(double num);


int main(void) {
	srand(time(NULL));

	double *numbers = new double[10];


	for (int i = 0; i < 10; i++) {
		numbers[i] = m_rand(-100, 100);
		cout << numbers[i] << endl;
	}

	double *p = numbers;

	m_sort(numbers, 10);

	cout << endl << endl << endl;

	for (int i = 0; i < 10; i++) {
		cout << numbers[i] << endl;
	}

	_getch();
	return 0;
}



double m_rand(double l, double r) {
	return ((double)rand() / RAND_MAX * (r - l) + l);
}

void m_sort(double *data, int len) {
	for (int i = 1; i<len; i++)
		for (int j = i; j>0 && get_fraction(data[j - 1])>get_fraction(data[j]); j--) 
			swap(data[j - 1], data[j]);

}

double get_fraction(double num) {
	num = abs(num);
	return (num - int(num));
}