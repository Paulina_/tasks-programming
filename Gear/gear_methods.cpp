#include "gear.h"
#include<iostream>
#include<fstream>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

// // Рекурсивная заливка дырки.
int Gear::fillArea(int x, int y, char value, char **data) {
	if (data[x][y] == value)
		return 0;

	data[x][y] = value;
	fillArea(x, y + 1, value, data);
	fillArea(x, y - 1, value, data);
	fillArea(x + 1, y, value, data);
	fillArea(x - 1, y, value, data);
}


//   Закраска старого узора.
void Gear::erasePattern(char **data) {
	double end = 2 * M_PI;
	int x, y;

	for (double q = 0; q <= end; q += 0.00001) {
		x = (this->radius[1])*cos(q) + MIDDLE + 1;
		y = (this->radius[1])*sin(q) + MIDDLE;
		this->fillArea(x, y, '1', data);
	}
}

//   Нанесение нового узора.
void Gear::createPatternt(char *pattern, char **data) {
	double end = 2 * M_PI;

	double hole_angle = 2 * M_PI / N; //  .
	double step = (this->radius[1] - this->radius[0]) / ((2 * M_PI / N) / 0.00001);



	int x_upper, y_upper, x_bottom, y_bottom;
	int x_l, y_l;
	int x_r, y_r;

	int counter = 0;
	char value = '0';

	for (double a = 0; a < end; a += hole_angle) {
		value = pattern[counter];
		counter++;
		double i = 0.0;
		//   Создаём контур дырки.
		if (value == '0') {
			for (double q = a; q <= (a + hole_angle); q += 0.00001) {
				x_upper = this->radius[1] * cos(q) + MIDDLE;
				y_upper = this->radius[1] * sin(q) + MIDDLE;
				data[x_upper][y_upper] = value;

				x_bottom = this->radius[0] * cos(q) + MIDDLE;
				y_bottom = this->radius[0] * sin(q) + MIDDLE;
				data[x_bottom][y_bottom] = value;


				x_l = (this->radius[1] - i)*cos(a) + MIDDLE;
				y_l = (this->radius[1] - i)*sin(a) + MIDDLE;
				data[x_l][y_l] = value;

				x_r = (this->radius[1] - i)*cos(a + hole_angle) + MIDDLE;
				y_r = (this->radius[1] - i)*sin(a + hole_angle) + MIDDLE;
				data[x_r][y_r] = value;

				i += step;
			}

			//Рекурсивно закрашиваем дырку.
			this->fillArea(
				(this->radius[1] - (this->radius[1] - this->radius[0]) / 2)*cos(a + hole_angle / 2) + MIDDLE,
				this->radius[1] * sin(a + hole_angle / 2) + MIDDLE,
				'0',
				data);
		}


	}
}

// Создание .dat файла с шестеренкой.
void Gear::drawGear(char **data) {
	std::ofstream os("gear.bmp", std::ios::binary);

	unsigned char signature[2] = { 'B', 'M' };
	unsigned int fileSize = 14 + 40 + 5570 * 5570 * 4;
	unsigned int reserved = 0;
	unsigned int offset = 14 + 40;

	unsigned int headerSize = 40;
	unsigned int dimensions[2] = { 5570, 5570 };
	unsigned short colorPlanes = 1;
	unsigned short bpp = 32;
	unsigned int compression = 0;
	unsigned int imgSize = 5570 * 5570 * 4;
	unsigned int resolution[2] = { 2795, 2795 };
	unsigned int pltColors = 0;
	unsigned int impColors = 0;

	os.write(reinterpret_cast<char*>(signature), sizeof(signature));
	os.write(reinterpret_cast<char*>(&fileSize), sizeof(fileSize));
	os.write(reinterpret_cast<char*>(&reserved), sizeof(reserved));
	os.write(reinterpret_cast<char*>(&offset), sizeof(offset));

	os.write(reinterpret_cast<char*>(&headerSize), sizeof(headerSize));
	os.write(reinterpret_cast<char*>(dimensions), sizeof(dimensions));
	os.write(reinterpret_cast<char*>(&colorPlanes), sizeof(colorPlanes));
	os.write(reinterpret_cast<char*>(&bpp), sizeof(bpp));
	os.write(reinterpret_cast<char*>(&compression), sizeof(compression));
	os.write(reinterpret_cast<char*>(&imgSize), sizeof(imgSize));
	os.write(reinterpret_cast<char*>(resolution), sizeof(resolution));
	os.write(reinterpret_cast<char*>(&pltColors), sizeof(pltColors));
	os.write(reinterpret_cast<char*>(&impColors), sizeof(impColors));

	unsigned char x, r, g, b;

	for (int i = 0; i < dimensions[1]; ++i)
	{
		for (int j = 0; j < dimensions[0]; ++j)
		{
			x = 0;
			if (data[i][j] == '0') {
				r = 255;
				g = 255;
				b = 255;
			}
			else {
				r = 0;
				g = 0;
				b = 0;
			}
			os.write(reinterpret_cast<char*>(&b), sizeof(b));
			os.write(reinterpret_cast<char*>(&g), sizeof(g));
			os.write(reinterpret_cast<char*>(&r), sizeof(r));
			os.write(reinterpret_cast<char*>(&x), sizeof(x));
		}
	}
	os.close();
}
