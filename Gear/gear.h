#ifndef _GEAR_H_
#define _GEAR_H_

const int N = 28;
const int COLUMNS = 5570;
const int ROWS = 5570;
const int MIDDLE = ROWS / 2;


class Gear {
public:
	int radius[2];
	int fillArea(int x, int y, char value, char **data); //  Рекурсивная заливка дырки.
	void erasePattern(char **data); //  Закраска старого узора. 
	void createPatternt(char *pattern, char **data); //   Нанесение нового узора.
	void drawGear(char **data); //  Создание .dat файла с шестеренкой.

};

#endif
