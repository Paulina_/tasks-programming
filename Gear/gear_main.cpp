#include<iostream>
#include<locale>
#include<fstream>

#pragma comment(linker, "/STACK:100000000000")

#include"gear.h"

using namespace std;

const char* FILENAME = "gear_template.txt";
char *pattern = "0100101000011001000001011101";

char** readFile();
void returnRadius(char **data, int *radii);

int main(void) {
	setlocale(LC_ALL, "RUSSIAN");
	char **data = readFile();
	int *radii = new int;

	radii[0] = 0;
	radii[1] = 0;

	returnRadius(data, radii);

	Gear *newGear = new Gear();

	newGear->radius[0] = radii[0];
	newGear->radius[1] = radii[1];

	//  Стираем старый узор.
	newGear->erasePattern(data);

	//   Создаём новый узор.
	newGear->createPatternt(pattern, data);

	newGear->drawGear(data);

	system("pause");
	return 0;
}

/* --------- Сбор данных из файла  --------- */
char** readFile() {
	ifstream in(FILENAME);

	char **data;
	data = new char*[ROWS]; // 
	for (int i = 0; i < ROWS; i++) {
		data[i] = new char[COLUMNS];
	}
	char ch;
	if (in.is_open())
	{
		int i = 1;
		do {
			in >> ch;
			i++;
		} while (i < 12);

		while (!in.eof()) {
			for (int i = 0; i < ROWS; i++) {
				for (int j = 0; j < COLUMNS; j++) {
					in >> data[i][j];
				}
			}
		}
		in.close();
	}

	else
	{
		cout << "  ." << endl;
		//system("pause");
	}

	return data;
}


/* --------- Поиск радиусов  --------- */

void returnRadius(char **data, int *radii) {
	bool r2 = 1, r3 = 1;

	for (int i = MIDDLE; i > 0; i--) {
		//   .
		if (data[MIDDLE][i] == '0' && data[MIDDLE][i + 1] == '1')
			r2 = 0;

		//  .
		if (data[MIDDLE][i] == '1' && data[MIDDLE][i + 1] == '0' && r2 == 0)
			break;

		if (r2 == 1)
			radii[0]++;

		if (r3 == 1)
			radii[1]++;
	}
}